// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

// For some reason it has to go here, NOT after pragma once
#define _CRT_SECURE_NO_WARNINGS

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>


// TODO: reference additional headers your program requires here

// Just include std::string becase we always use it
#include <string>
using string = std::string;

#define __WXMSW__

#include "wx/setup.h"
#include "wx/wx.h"

#include "lua/lua_funcs.h"
#include "lua/xlua.h"
