#include "stdafx.h"

static void tag_error(lua_State *L, int narg, int tag) {
	luaL_typerror(L, narg, lua_typename(L, tag));
}

static string get_called_function(lua_State *L) {
	string str = "<unknown>";

	lua_getglobal(L, "EWS");
	if (!lua_isnil(L, -1)) {
		lua_getfield(L, -1, "_internal_get_error");
		if (!lua_isnil(L, -1)) {
			lua_pushvalue(L, -1); // Duplicate EWS for 'this'
			lua_call(L, 1, 1);

			str = lua_tostring(L, -1);
		}
		lua_pop(L, 1);
	}
	lua_pop(L, 1);

	return str;
}

// Note these are modified to support an system

int luaX_newmetatable(lua_State *L, const char *tname) {
	lua_getfield(L, LUA_REGISTRYINDEX, tname);  /* get registry.name */
	if (!lua_isnil(L, -1))  /* name already in use? */
		return 0;  /* leave previous value on top, but return 0 */
	lua_pop(L, 1);
	lua_newtable(L);  /* create metatable */
	lua_pushvalue(L, -1);
	lua_setfield(L, LUA_REGISTRYINDEX, tname);  /* registry.name = metatable */

	// Custom stuff - inheritence
	lua_pushstring(L, tname);
	lua_setfield(L, -2, "__name");

	lua_newtable(L);
	lua_setfield(L, -2, "___inheritance");

	return 1;
}

void luaX_addparent(lua_State *L, const char *tname) {
	lua_getfield(L, -1, "___inheritance");
	lua_pushboolean(L, true);
	lua_setfield(L, -2, tname);
	lua_pop(L, 1);
}

bool luaX_isudata(lua_State *L, int ud, const char *tname) {
	if (lua_type(L, ud) != LUA_TUSERDATA) return false;

	void *p = lua_touserdata(L, ud);
	if (p == NULL) return false;

	// Only pushes to the stack if it returns true
	if (!lua_getmetatable(L, ud)) return false;

	lua_getfield(L, -1, "___inheritance");
	if (!lua_istable(L, -1)) {
		lua_pop(L, 2);
		return false;
	}

	lua_getfield(L, -1, tname);

	if (lua_isboolean(L, -1) && lua_toboolean(L, -1)) {
		lua_pop(L, 3);
		return true;
	}

	lua_pop(L, 3);
	return false;
}

void *luaX_checkudata(lua_State *L, int ud, const char *tname) {
	void *p = lua_touserdata(L, ud);
	if (p != NULL) {  /* value is a userdata? */
		if (lua_getmetatable(L, ud)) {  /* does it have a metatable? */
			//lua_getfield(L, LUA_REGISTRYINDEX, tname);  /* get correct metatable */
			//if (lua_equal(L, -1, -2)) {  /* does it have the correct mt? */ // Was lua_rawequal, but I haven't yet located that (and it possibly doesn't exist)
			//	lua_pop(L, 2);  /* remove both metatables */
			//	return p;
			//}

			lua_getfield(L, -1, "___inheritance");
			if (lua_istable(L, -1)) {
				lua_getfield(L, -1, tname);

				if (lua_isboolean(L, -1) && lua_toboolean(L, -1)) {
					lua_pop(L, 3);
					return p;
				}
				else {
					lua_getfield(L, -3, "__name");
					const char *type = lua_tostring(L, -1);
					if (!type) type = "<unknown>";

					string func = get_called_function(L);

					luaL_error(L, "Invalid self value - wanted %s, got %s (in func %s)", tname, type, func.c_str());  /* else error */
				}
			}
		}
	}
	luaL_checktype(L, ud, LUA_TUSERDATA);
	luaL_error(L, "Invalid self value (unknown reason)");  /* else error */
	return NULL;  /* to avoid warnings */
}


void *luaX_checkudata_nonil(lua_State *L, int ud, const char *tname) {
	void *data = luaX_checkudata(L, ud, tname);

	if (!data) luaL_error(L, "Invalid (corrupted?) userdata NULL!");

	return data;
}

int luaX_checkoptionmapped(lua_State * L, int narg, const char * def, const char * const lst[], int const mapping[]) {
	int value = luaL_checkoption(L, narg, def, lst);

	return mapping[value];
}

int luaL_argerror(lua_State * L, int narg, const char * extramsg) {
	string func = get_called_function(L);

	return luaL_error(L, "bad argument #%d (%s) in %s", narg, extramsg, func.c_str());
}

int luaL_checkoption(lua_State * L, int narg, const char * def, const char * const lst[]) {
	const char *name = (def) ? luaL_optstring(L, narg, def) :
		luaL_checkstring(L, narg);
	int i;
	for (i = 0; lst[i]; i++)
		if (strcmp(lst[i], name) == 0)
			return i;

	char buff[1024];
	snprintf(buff, sizeof(buff), "invalid option " LUA_QS, name);
	return luaL_argerror(L, narg, buff);
}

int luaL_typerror(lua_State * L, int narg, const char * tname) {
	char msg[1024];
	snprintf(msg, sizeof(msg), "%s expected, got %s", tname, luaL_typename(L, narg));
	return luaL_argerror(L, narg, msg);
}

void luaL_checktype(lua_State * L, int narg, int t) {
	if (lua_type(L, narg) != t)
		tag_error(L, narg, t);
}

void luaL_checkany(lua_State * L, int narg) {
	if (lua_type(L, narg) == LUA_TNONE)
		luaL_argerror(L, narg, "value expected");
}

const char * luaL_checklstring(lua_State * L, int narg, size_t * len) {
	const char *s = lua_tolstring(L, narg, len);
	if (!s) tag_error(L, narg, LUA_TSTRING);
	return s;
}

const char * luaL_optlstring(lua_State * L, int narg, const char * def, size_t * len) {
	if (lua_isnoneornil(L, narg)) {
		if (len)
			*len = (def ? strlen(def) : 0);
		return def;
	}
	else return luaL_checklstring(L, narg, len);
}

lua_Number luaL_checknumber(lua_State * L, int narg) {
	lua_Number d = lua_tonumber(L, narg);
	if (d == 0 && !lua_isnumber(L, narg))  /* avoid extra test when d is not 0 */
		tag_error(L, narg, LUA_TNUMBER);
	return d;
}

lua_Number luaL_optnumber(lua_State * L, int narg, lua_Number def) {
	return luaL_opt(L, luaL_checknumber, narg, def);
}

lua_Integer luaL_checkinteger(lua_State * L, int narg) {
	lua_Integer d = lua_tointeger(L, narg);
	if (d == 0 && !lua_isnumber(L, narg))  /* avoid extra test when d is not 0 */
		tag_error(L, narg, LUA_TNUMBER);
	return d;
}

lua_Integer luaL_optinteger(lua_State * L, int narg, lua_Integer def) {
	return luaL_opt(L, luaL_checkinteger, narg, def);
}