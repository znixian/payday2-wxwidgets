// LibLuaEWS.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#define INIT_FUNC // We want to set everything up in this file
#include "lua/lua_funcs.h"

#include "wx/evtloop.h"

#include "EWS/Frame.h"
#include "EWS/BoxSizer.h"
#include "EWS/Panel.h"
#include "EWS/Menus.h"
#include "EWS/LuaWXEvent.h"
#include "EWS/AppWindow.h"
#include "EWS/Button.h"
#include "EWS/Notebook.h"
#include "EWS/ListCtrl.h"

#define SUBHOOK_STATIC
#include <subhook/subhook.h>

//wxDEFINE_TIED_SCOPED_PTR_TYPE(wxEventLoopBase)

typedef void*(*lua_access_func)(const char*);

class MyEventLoop : public wxGUIEventLoop {
public:
	void Step_Run() {
		// this is the event loop itself

		// give them the possibility to do whatever they want
		OnNextIteration();

		// generate and process idle events for as long as we don't
		// have anything else to do
		while (!m_shouldExit && ProcessIdle());

		if (!Pending()) return;

		if (m_shouldExit)
			throw "m_shouldExit";

		// a message came or no more idle processing to do, dispatch
		// all the pending events and call Dispatch() to wait for the
		// next message
		if (!ProcessEvents()) {
			// we got WM_QUIT
			throw "we got WM_QUIT";
		}
	}

	bool ProcessEvents() {
		// process pending wx events first as they correspond to low-level events
		// which happened before, i.e. typically pending events were queued by a
		// previous call to Dispatch() and if we didn't process them now the next
		// call to it might enqueue them again (as happens with e.g. socket events
		// which would be generated as long as there is input available on socket
		// and this input is only removed from it when pending event handlers are
		// executed)
		if (wxTheApp) {
			wxTheApp->ProcessPendingEvents();

			// One of the pending event handlers could have decided to exit the
			// loop so check for the flag before trying to dispatch more events
			// (which could block indefinitely if no more are coming).
			if (m_shouldExit)
				return false;
		}

		return Dispatch();
	}
};

class MyApp : public wxApp {
public:
	virtual bool OnInit();

	void Setup_Loop();

	//private:
	MyEventLoop * mel;
};

MyApp* app;

bool MyApp::OnInit() {
	//ews::Frame *frame = new ews::Frame("Hello World", wxPoint(50, 50), wxSize(450, 340));
	//frame->Show(true);
	return true;
}

void MyApp::Setup_Loop() {
	m_mainLoop = mel = new MyEventLoop;
	wxEventLoopBase::SetActive(mel);

	OnLaunched();
}

int hello(lua_State *L) {
	PD2HOOK_LOG_LOG("Hello, setting up frame");

	if (!app) {
		app = new MyApp();
		wxApp::SetInstance(app);
		wxEntryStart(0, NULL);
		app->CallOnInit();

		app->Setup_Loop();
	}

	//app->OnExit();
	//wxEntryCleanup();

	return 0;
}

subhook::Hook myhook;

HWND
WINAPI
hook_window(
	_In_ DWORD dwExStyle,
	_In_opt_ LPCWSTR lpClassName,
	_In_opt_ LPCWSTR lpWindowName,
	_In_ DWORD dwStyle,
	_In_ int X,
	_In_ int Y,
	_In_ int nWidth,
	_In_ int nHeight,
	_In_opt_ HWND hWndParent,
	_In_opt_ HMENU hMenu,
	_In_opt_ HINSTANCE hInstance,
	_In_opt_ LPVOID lpParam) {

	subhook::ScopedHookRemove _remove(&myhook); // Subhook's trampolines don't usually work

	if (!lstrcmp(L"PAYDAY 2", lpWindowName)) {
		PD2HOOK_LOG_LOG("Hooked window creation!");

		wxFrame *myframe = new wxFrame(NULL, wxID_ANY, "My PD2 Window", wxPoint(50, 50), wxSize(800, 600));

		wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);
		topsizer->Add(
			new wxTextCtrl(myframe, -1, "My text.", wxDefaultPosition, wxSize(100, 60), wxTE_MULTILINE),
			0,            // make vertically unstretchable
			0, //wxEXPAND |    // make horizontally stretchable
			//wxALL,        //   and make border all around
			10);         // set border width to 10

		wxPanel *panel = new wxPanel(myframe);
		topsizer->Add(
			panel,
			1,
			wxEXPAND
		);

		myframe->SetSizerAndFit(topsizer);
		myframe->Show(true);

		HWND id = panel->GetHandle();
		return id;
	}

	return CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

extern "C" __declspec(dllexport) void SuperBLT_Plugin_Setup(lua_access_func get_exposed_function) {
	app = new MyApp();
	wxApp::SetInstance(app);
	wxEntryStart(0, NULL);
	app->CallOnInit();

	app->Setup_Loop();

	// This would put PD2 in a custom window
	// Breaks input, so don't do it for now
	//myhook.Install(CreateWindowExW, hook_window);

	// Load all our Lua functions
	for (const auto &func_ptr : all_lua_funcs_list) {
		AutoFuncSetup &func = *func_ptr;
		*func.ptr = get_exposed_function(func.name);
	}

	PD2HOOK_LOG_LOG("EWS Plugin: Init");
}

extern "C" __declspec(dllexport) void SuperBLT_Plugin_Init_State(lua_State *L) {
	lua_newtable(L);

	// Set the metatable
	lua_newtable(L);
	lua_setmetatable(L, -2);

	lua_pushcfunction(L, hello);
	lua_setfield(L, -2, "hello");

	int orig_stack = lua_gettop(L);

	Frame::AddLua(L);
	BoxSizer::AddLua(L);
	Panel::AddLua(L);
	MenuBar::AddLua(L);
	Menu::AddLua(L);
	LuaWXEvent::AddLua(L);
	AppWindow::AddLua(L);
	Button::AddLua(L);
	Notebook::AddLua(L);
	ListCtrl::AddLua(L);

	assert(lua_gettop(L) == orig_stack);

	lua_setglobal(L, "EWS");
}

extern "C" __declspec(dllexport) void SuperBLT_Plugin_Update() {
	if (app) {
		//app->mel->Step_Run();

		wxYield();
		WidgetBase::DispatchLuaEventQueue();
	}
}

wxIMPLEMENT_WX_THEME_SUPPORT
