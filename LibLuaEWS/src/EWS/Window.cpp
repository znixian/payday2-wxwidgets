#include "stdafx.h"
#include "EWS/Window.h"
#include "EWS/BoxSizer.h"
#include "PD2Types.h"

#include "wx/listctrl.h"

const char *const Window::KEY = MT_KEY("Panel");

const char *Window::STYLE_FLAGS_NAMES[] = {
	// Upper bits, TODO move these into a common header as they're used on all wxWindow-s
	"FULL_REPAINT_ON_RESIZE",
	"POPUP_WINDOW",
	"WANTS_CHARS",
	"TAB_TRAVERSAL",
	"TRANSPARENT_WINDOW",
	"BORDER_NONE",
	"CLIP_CHILDREN",
	"ALWAYS_SHOW_SB",
	"BORDER_STATIC",
	"BORDER_SIMPLE",
	"BORDER_RAISED",
	"BORDER_SUNKEN",
	"SUNKEN_BORDER",
	"BORDER_THEME",
	"CAPTION", // Uses same address as next
	"CLIP_SIBLINGS",
	"HSCROLL",
	"VSCROLL",

	// Lower bits, AFAIK frame specific
	"CENTRE",
	"FRAME_NO_TASKBAR",
	"FRAME_TOOL_WINDOW",
	"FRAME_FLOAT_ON_PARENT",
	"FRAME_SHAPED",
	"DIALOG_NO_PARENT",
	"RESIZE_BORDER",
	"TINY_CAPTION_VERT",
	"MAXIMIZE_BOX",
	"MINIMIZE_BOX",
	"SYSTEM_MENU",
	"CLOSE_BOX",
	"MAXIMIZE",
	"MINIMIZE",
	"STAY_ON_TOP",

	"DEFAULT_FRAME_STYLE",

	// ListCtrl
	"LC_REPORT",
	"LC_SINGLE_SEL",

	NULL
};

int Window::STYLE_FLAGS_MASKS[] = {
	// Upper bits
	wxFULL_REPAINT_ON_RESIZE,
	wxPOPUP_WINDOW,
	wxWANTS_CHARS,
	wxTAB_TRAVERSAL,
	wxTRANSPARENT_WINDOW,
	wxBORDER_NONE,
	wxCLIP_CHILDREN,
	wxALWAYS_SHOW_SB,
	wxBORDER_STATIC,
	wxBORDER_SIMPLE,
	wxBORDER_RAISED,
	wxBORDER_SUNKEN,
	wxSUNKEN_BORDER,
	wxBORDER_THEME,
	wxCAPTION, // Uses same address as next
	wxCLIP_SIBLINGS,
	wxHSCROLL,
	wxVSCROLL,

	// Lower bits
	wxCENTRE,
	wxFRAME_NO_TASKBAR,
	wxFRAME_TOOL_WINDOW,
	wxFRAME_FLOAT_ON_PARENT,
	wxFRAME_SHAPED,
	wxDIALOG_NO_PARENT,
	wxRESIZE_BORDER,
	wxTINY_CAPTION_VERT,
	wxMAXIMIZE_BOX,
	wxMINIMIZE_BOX,
	wxSYSTEM_MENU,
	wxCLOSE_BOX,
	wxMAXIMIZE,
	wxMINIMIZE,
	wxSTAY_ON_TOP,

	wxDEFAULT_FRAME_STYLE,

	// ListCtrl
	wxLC_REPORT,
	wxLC_SINGLE_SEL,

	NULL
};

void Window::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	WidgetBase::AddMembers(L);

#define FUNC(name) \
lua_pushcfunction(L, (lua_invoke<Window, &Window::name>)); \
lua_setfield(L, -2, #name)

	FUNC(set_visible);
	FUNC(set_sizer);
	FUNC(set_position);
	FUNC(freeze);
	FUNC(thaw);
	FUNC(refresh);
	FUNC(fit);

#undef FUNC
}

int Window::set_visible(lua_State * L) {
	GetWrappedObject()->Show(lua_toboolean(L, 1));
	return 0;
}

int Window::set_sizer(lua_State * L) {
	BoxSizer *sizer = *(BoxSizer**)luaX_checkudata_nonil(L, 1, BoxSizer::KEY);

	// Should we use SetSizerAndFit?
	GetWrappedObject()->SetSizer(sizer->GetWrappedObject());

	return 0;
}

int Window::set_position(lua_State * L) {
	luaL_checktype(L, 1, LUA_TUSERDATA);
	dslScriptVector3 *vec = (dslScriptVector3*)lua_touserdata(L, 1);

	GetWrappedObject()->SetPosition(*vec);

	return 0;
}

int Window::DecodeFlagsString(lua_State *L, const char *flags_str) {
	int decoration_flags = 0;

	// Go through the flag string
	for (const char *cp = flags_str; *cp;) {
		// Find the number of characters until the next ',' or the end of the string
		int len = 0;
		for (; ; len++) {
			char next = *(cp + len);
			if (!next || next == ',') break;
		}

		// Search for the correct flag
		const char **tags = STYLE_FLAGS_NAMES;
		for (int i = 0; tags[i]; i++) {
			if (strlen(tags[i]) == len && !strncmp(tags[i], cp, len)) {
				decoration_flags |= STYLE_FLAGS_MASKS[i];
				goto found;
			}
		}

		// Give an error if we can't find it
		char buff[1024];
		strncpy(buff, cp, len);
		buff[len] = NULL;
		luaL_error(L, "Unknown decoration flag: %s", buff);

		// If found, continue by advancing to the start of the next string
	found:
		cp += len;

		// If we have reached another comma, go past it to the next argument
		if (*cp == ',') cp++;
	}

	return decoration_flags;
}

int Window::freeze(lua_State *L) {
	GetWrappedObject()->Freeze();
	return 0;
}

int Window::thaw(lua_State *L) {
	GetWrappedObject()->Thaw();
	return 0;
}

int Window::refresh(lua_State *L) {
	GetWrappedObject()->Refresh();
	return 0;
}

int Window::fit(lua_State *L) {
	GetWrappedObject()->Fit();
	return 0;
}
