#include "stdafx.h"
#include "EWS/Frame.h"
#include "EWS/Menus.h"
#include "PD2Types.h"

const char *const Frame::KEY = MT_KEY("Frame");

Frame::Frame(lua_State *L) {
	string title = luaL_checkstring(L, 1);

	luaL_checktype(L, 2, LUA_TUSERDATA);
	wxPoint position = *(dslScriptVector3*)lua_touserdata(L, 2);

	luaL_checktype(L, 3, LUA_TUSERDATA);
	wxSize size = *(dslScriptVector3*)lua_touserdata(L, 3);

	// Break down something like CAPTION,SYSTEM_MENU,MINIMIZE_BOX,CLOSE_BOX
	const char *flags_str = luaL_checkstring(L, 4);
	int decoration_flags = DecodeFlagsString(L, flags_str);

	// Optional parent window
	wxFrame *parent = NULL;
	if (!lua_isnoneornil(L, 5)) {
		parent = *luaX_checkudata_cast<Frame>(L, 5);
	}

	WrappedWindowWritable() = new wxFrame(parent, wxID_ANY, title, position, size, decoration_flags);
}

Frame::~Frame() {
}

void Frame::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<Frame>);
	lua_setfield(L, -2, "Frame");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void Frame::AddMembers(lua_State * L) {
	luaX_addparent(L, Frame::KEY);
	Window::AddMembers(L);

#define FUNC(name) REGISTER_LUA_FUNC(Frame, name)
	FUNC(set_menu_bar);
	FUNC(set_focus);
	FUNC(set_icon);
#undef FUNC
}

int Frame::set_menu_bar(lua_State * L) {
	MenuBar *menu = luaX_checkudata_cast<MenuBar>(L, 1);

	GetWrappedObject()->SetMenuBar(menu->GetWrappedObject());

	return 0;
}

int Frame::set_focus(lua_State * L) {
	GetWrappedObject()->SetFocus();

	return 0;
}

int Frame::set_icon(lua_State * L) {
	// TODO global icon loading

	wxIcon icon(luaL_checkstring(L, 1));

	GetWrappedObject()->SetIcon(icon);

	return 0;
}
