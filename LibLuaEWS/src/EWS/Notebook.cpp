#include "stdafx.h"
#include "EWS/Notebook.h"

const char *const Notebook::KEY = MT_KEY("Notebook");

Notebook::Notebook(lua_State *L) {
	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);
	wxWindow *parent = other->GetWrappedObject();
	assert(parent);

	int id = wxID_ANY;
	string id_str = luaL_checkstring(L, 2);
	if (!id_str.empty()) {
		id = StringToId(id_str, true);
	}

	// Break down something like CAPTION,SYSTEM_MENU,MINIMIZE_BOX,CLOSE_BOX
	const char *flags_str = luaL_checkstring(L, 3);
	int flags = DecodeFlagsString(L, flags_str);

	WrappedWindowWritable() = new wxNotebook(parent, id, wxDefaultPosition, wxDefaultSize, flags);
}

void Notebook::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<Notebook>);
	lua_setfield(L, -2, "Notebook");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void Notebook::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);

	// Unimplemented methods from Lua (may not be complete):
	/*
	notebook:set_enabled
	notebook:set_min_size (probably in Window)
	*/

#define FUNC(name) REGISTER_LUA_FUNC(Notebook, name)
	FUNC(add_page);
	FUNC(delete_page);
	FUNC(delete_all_pages); // Not present in Lua, but it might be handy for modders using this
	FUNC(get_page);
	FUNC(get_page_text);
	FUNC(set_page); // Should be called 'select_page'
	FUNC(set_page_text);
	FUNC(get_current_page);
	FUNC(get_page_count);
#undef FUNC
}

int Notebook::add_page(lua_State *L) {
	// Get the page window
	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);
	wxWindow *page = other->GetWrappedObject();
	assert(page);

	// Text and selected
	string text = luaL_checkstring(L, 2);
	bool selected = lua_toboolean(L, 3);

	GetWrappedObject()->AddPage(page, text, selected);

	return 0;
}

int Notebook::delete_page(lua_State *L) {
	int page = luaL_checkint(L, 1);
	GetWrappedObject()->DeletePage(page);
	return 0;
}

int Notebook::delete_all_pages(lua_State *L) {
	GetWrappedObject()->DeleteAllPages();
	return 0;
}

int Notebook::get_page(lua_State * L) {
	int page = luaL_checkint(L, 1);

	wxWindow *w = GetWrappedObject()->GetPage(page);

	wxWindow **ptr = (wxWindow**)lua_newuserdata(L, sizeof(wxWindow*));
	*ptr = w;

	return 1;
}

int Notebook::get_page_text(lua_State * L) {
	int page = luaL_checkint(L, 1);
	wxString text = GetWrappedObject()->GetPageText(page);
	lua_pushstring(L, text.c_str());
	return 1;
}

int Notebook::set_page(lua_State * L) {
	int page = luaL_checkint(L, 1);
	int previous = GetWrappedObject()->SetSelection(page);
	lua_pushinteger(L, previous);
	return 1;
}

int Notebook::set_page_text(lua_State * L) {
	int page = luaL_checkint(L, 1);
	string text = luaL_checkstring(L, 2);
	GetWrappedObject()->SetPageText(page, text);
	return 0;
}

int Notebook::get_current_page(lua_State * L) {
	wxWindow *w = GetWrappedObject()->GetCurrentPage();

	wxWindow **ptr = (wxWindow**)lua_newuserdata(L, sizeof(wxWindow*));
	*ptr = w;

	return 1;
}

int Notebook::get_page_count(lua_State * L) {
	int count = GetWrappedObject()->GetPageCount();
	lua_pushinteger(L, count);
	return 1;
}
