#include "stdafx.h"
#include "EWS/Window.h"
#include "EWS/BoxSizer.h"

const char *const BoxSizer::KEY = MT_KEY("BoxSizer");

BoxSizer::BoxSizer(lua_State *L) {
	static const char* type_opt[] = {
		"HORIZONTAL", "VERTICAL", NULL
	};
	static int type_opt_map[] = {
		wxHORIZONTAL, wxVERTICAL
	};

	WrappedObjectWritable() = new wxBoxSizer(
		luaX_checkoptionmapped(L, 1, NULL, type_opt, type_opt_map)
	);
}

BoxSizer::~BoxSizer() {
}

void BoxSizer::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<BoxSizer>);
	lua_setfield(L, -2, "BoxSizer");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void BoxSizer::AddMembers(lua_State * L) {
	luaX_addparent(L, BoxSizer::KEY);
	WidgetBase::AddMembers(L);

	lua_pushcfunction(L, (lua_invoke<BoxSizer, &BoxSizer::add>));
	lua_setfield(L, -2, "add");
}

int BoxSizer::add(lua_State * L) {
	int proportion = luaL_optinteger(L, 2, 0);

	// This should be arg4, but it seems PD2 shuffled things to use the last arg as a vararg for the flags
	int border = luaL_optinteger(L, 3, 0);

	static const char* flags_keys[] = {
		"TOP","BOTTOM","LEFT","RIGHT","ALL",

		"EXPAND","SHAPED","FIXED_MINSIZE","RESERVE_SPACE_EVEN_IF_HIDDEN",

		"ALIGN_CENTER","ALIGN_CENTRE","ALIGN_LEFT","ALIGN_RIGHT","ALIGN_TOP",
		"ALIGN_BOTTOM","ALIGN_CENTER_VERTICAL","ALIGN_CENTRE_VERTICAL",
		"ALIGN_CENTER_HORIZONTAL","ALIGN_CENTRE_HORIZONTAL",

		NULL
	};
	static int flags_map[] = {
		wxTOP,wxBOTTOM,wxLEFT,wxRIGHT,wxALL,
		
		wxEXPAND,wxSHAPED,wxFIXED_MINSIZE,wxRESERVE_SPACE_EVEN_IF_HIDDEN,
		
		wxALIGN_CENTER,wxALIGN_CENTRE,wxALIGN_LEFT,wxALIGN_RIGHT,wxALIGN_TOP,
		wxALIGN_BOTTOM,wxALIGN_CENTER_VERTICAL,wxALIGN_CENTRE_VERTICAL,
		wxALIGN_CENTER_HORIZONTAL,wxALIGN_CENTRE_HORIZONTAL,
	};

	int flags = 0;

	for (int i = 4; i <= lua_gettop(L); i++) {
		flags |= luaX_checkoptionmapped(L, i, NULL, flags_keys, flags_map);
	}

	if (luaX_isudata(L, 1, BoxSizer::KEY)) {
		BoxSizer *other = *(BoxSizer**)luaX_checkudata(L, 1, BoxSizer::KEY);
		GetWrappedObject()->Add(other->GetWrappedObject(), proportion, flags, border);
	}
	else {
		Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);
		GetWrappedObject()->Add(other->GetWrappedObject(), proportion, flags, border);
	}

	return 0;
}
