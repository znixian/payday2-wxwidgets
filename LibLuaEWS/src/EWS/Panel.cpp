#include "stdafx.h"
#include "EWS/Panel.h"

const char *const Panel::KEY = MT_KEY("Panel");

Panel::Panel(lua_State *L) {
	static const char* type_opt[] = {
		"HORIZONTAL", "VERTICAL", NULL
	};
	static int type_opt_map[] = {
		wxHORIZONTAL, wxVERTICAL
	};

	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);

	if (other) {
		wxWindow * parent = other->GetWrappedObject();
		assert(parent);
		WrappedWindowWritable() = new wxPanel(parent);
	}
	else {
		WrappedWindowWritable() = new wxPanel();
	}

}

void Panel::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<Panel>);
	lua_setfield(L, -2, "Panel");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void Panel::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);
}
