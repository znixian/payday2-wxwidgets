#include "stdafx.h"
#include "EWS/LuaWXEvent.h"

const char *const LuaWXEvent::KEY = MT_KEY("LuaWXEvent");

void LuaWXEvent::AddLua(lua_State * L) {
	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

LuaWXEvent::LuaWXEvent(string id) : id(id) {
}

void LuaWXEvent::AddMembers(lua_State * L) {
	luaX_addparent(L, LuaWXEvent::KEY);

#define FUNC(name) REGISTER_LUA_FUNC(LuaWXEvent, name)
	FUNC(get_id);
#undef FUNC
}

int LuaWXEvent::get_id(lua_State * L) {
	lua_pushstring(L, id.c_str());
	return 1;
}
