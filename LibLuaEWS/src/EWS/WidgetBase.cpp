#include "stdafx.h"
#include "EWS/WidgetBase.h"
#include "EWS/LuaWXEvent.h"
#include "WXEvents.h"
#include <list>
#include <map>
#include <mutex>

using namespace events;

const char *WidgetBase::KEY = MT_KEY("WidgetBase");

int WidgetBase::StringToId(string name, bool shouldRegister) {
	static std::map<string, int> ids;
	static int next_id = 1000; // Start from some random high number, in case we ever want to use the lower ones

	if (ids.count(name)) {
		// It turns out that IDs can be reused for multiple components
		// TODO seperation of IDs between heirachies

		//if (shouldRegister) {
		//	throw "Cannot re-register item " + name;
		//}

		return ids.at(name);
	}

	if (!shouldRegister)
		return 0;

	int this_id = next_id++;
	ids[name] = this_id;
	return this_id;
}

WidgetBase::WidgetBase() {
}

void WidgetBase::AddMembers(lua_State * L) {
	luaX_addparent(L, WidgetBase::KEY);

	lua_pushcfunction(L, (lua_invoke<WidgetBase, &WidgetBase::lua_connect>));
	lua_setfield(L, -2, "connect");
}

WidgetBase::~WidgetBase() {
}

int WidgetBase::lua_connect(lua_State *L) {
	int argid = 1;

	// If two strings are supplied, the first one is an ID
	string id_string = "";
	if (lua_isstring(L, 2)) {
		id_string = luaL_checkstring(L, argid++);
	}

	string eventString = luaL_checkstring(L, argid++); // TODO use checkoption

	EventUserdata *data = new EventUserdata;
	data->L = L;
	data->idString = id_string;
	luaL_checktype(L, argid, LUA_TFUNCTION);
	lua_pushvalue(L, argid++);
	data->callback = luaL_ref(L, LUA_REGISTRYINDEX);

	lua_pushvalue(L, argid++);
	data->arg = luaL_ref(L, LUA_REGISTRYINDEX); // Not sure we need to do this as I think only strings and numbers are passed, but whatever.

	int id = wxID_ANY;
	if (!id_string.empty()) {
		id = StringToId(id_string, false);
	}

	if (!GetWXEvents().count(eventString)) {
		luaL_error(L, "Unknown event %s", eventString.c_str());
	}

	wxEventType eventId = GetWXEvents().at(eventString);

	assert(GetWrappedEvtHandler(), "No wrapped event handler in lua_connect!");

	// TODO are we supposed to disconnect any previous callbacks ore anything like that?

	GetWrappedEvtHandler()->Bind(eventId, &WidgetBase::HandleEvent, this, id, wxID_ANY, data);

	// TODO
	return 0;
}

class FiredEvent {
public:
	EventUserdata * eventData;

	// TODO add stuff from the event
};

std::list<FiredEvent*> eventQueue;
std::mutex eventQueueLock;

void WidgetBase::HandleEvent(wxEvent &event) {
	EventUserdata *data = (EventUserdata*)event.GetEventUserData();

	FiredEvent *l_event = new FiredEvent;
	l_event->eventData = data;

	if (!data->idString.empty())
		assert(StringToId(data->idString, false) == event.GetId(), "Event ID String/Num mismatch!");

	// Thread-safely add this event
	{
		std::lock_guard<std::mutex> lock(eventQueueLock);
		eventQueue.push_back(l_event);
	}
}

void WidgetBase::DispatchLuaEventQueue() {
	std::list<FiredEvent*> events;

	// Thread-safely grab the list of events, clearing the old list
	{
		std::lock_guard<std::mutex> lock(eventQueueLock);
		eventQueue.swap(events);
	}

	for (FiredEvent *event : events) {
		EventUserdata *data = event->eventData;
		lua_State *L = data->L;

		if (!is_active_state(L)) {
			PD2HOOK_LOG_WARN("Event discarded due to deactivated state");
			continue;
		}

		// Push the function and 1st argument
		lua_rawgeti(L, LUA_REGISTRYINDEX, data->callback);
		luaL_checktype(L, -1, LUA_TFUNCTION);
		lua_rawgeti(L, LUA_REGISTRYINDEX, data->arg);

		// Build the event object
		// For now, it's just a table. Later it should be a userdata.
		LuaWXEvent *luaEvent = new LuaWXEvent(data->idString);
		luaX_push_cxxobject<LuaWXEvent>(L, luaEvent);

		// Call the function
		lua_call(L, 2, 0);

		// The stack should be back to how it was when it started
	}
}
