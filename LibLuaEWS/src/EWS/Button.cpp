#include "stdafx.h"
#include "EWS/Button.h"

const char *const Button::KEY = MT_KEY("Button");

Button::Button(lua_State *L) {
	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);
	wxWindow * parent = other->GetWrappedObject();
	assert(parent);

	string label = luaL_checkstring(L, 2);

	int id = wxID_ANY;
	string id_str = luaL_checkstring(L, 3);
	if (!id_str.empty()) {
		id = StringToId(id_str, true);
	}

	// TODO styles

	WrappedWindowWritable() = new wxButton(parent, id, label);

}

void Button::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<Button>);
	lua_setfield(L, -2, "Button");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void Button::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);
}
