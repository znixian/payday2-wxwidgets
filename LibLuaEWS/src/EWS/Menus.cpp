#include "stdafx.h"
#include "EWS/Menus.h"

// MenuBar
const char *const MenuBar::KEY = MT_KEY("MenuBar");

MenuBar::MenuBar(lua_State *L) {
	WrappedWindowWritable() = new wxMenuBar();
}

void MenuBar::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<MenuBar>);
	lua_setfield(L, -2, "MenuBar");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void MenuBar::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);

#define FUNC(name) REGISTER_LUA_FUNC(MenuBar, name)
	FUNC(append);
#undef FUNC
}

int MenuBar::append(lua_State * L) {
	Menu *menu = luaX_checkudata_cast<Menu>(L, 1);
	string text = luaL_checkstring(L, 2);

	GetWrappedObject()->Append(menu->GetWrappedObject(), text);

	return 0;
}

// Menu
const char *const Menu::KEY = MT_KEY("Menu");

Menu::Menu(lua_State *L) {
	WrappedObjectWritable() = new wxMenu();
}

void Menu::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<Menu>);
	lua_setfield(L, -2, "Menu");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void Menu::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	WidgetBase::AddMembers(L);

#define FUNC(name) REGISTER_LUA_FUNC(Menu, name)
	FUNC(append_item);
	FUNC(append_separator);
	FUNC(append_check_item);
	FUNC(append_radio_item);
	FUNC(append_menu);
	FUNC(is_checked);
	FUNC(set_checked);
#undef FUNC
}

int Menu::append_item(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), true);
	string text = luaL_checkstring(L, 2);
	const char *help = luaL_optstring(L, 3, NULL);

	GetWrappedObject()->Append(id, text, help ? wxString(help) : wxString(wxEmptyString));

	return 0;
}

int Menu::append_separator(lua_State * L) {
	GetWrappedObject()->AppendSeparator();

	return 0;
}

int Menu::append_check_item(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), true);
	string text = luaL_checkstring(L, 2);
	const char *help = luaL_optstring(L, 3, NULL);

	GetWrappedObject()->AppendCheckItem(id, text, help ? wxString(help) : wxString(wxEmptyString));

	return 0;
}

int Menu::append_radio_item(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), true);
	string text = luaL_checkstring(L, 2);
	const char *help = luaL_optstring(L, 3, NULL);

	GetWrappedObject()->AppendRadioItem(id, text, help ? wxString(help) : wxString(wxEmptyString));

	return 0;
}

int Menu::append_menu(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), true); // We're not going to use it, but whatever it's supplied by Lua
	string text = luaL_checkstring(L, 2);
	Menu *menu = luaX_checkudata_cast<Menu>(L, 3);
	string help = luaL_checkstring(L, 4);

	GetWrappedObject()->AppendSubMenu(*menu, text, help);

	return 0;
}

int Menu::is_checked(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), false);

	bool state = GetWrappedObject()->IsChecked(id);

	lua_pushboolean(L, state);
	return 1;
}

int Menu::set_checked(lua_State * L) {
	int id = StringToId(luaL_checkstring(L, 1), false);
	bool state = lua_toboolean(L, 2);

	GetWrappedObject()->Check(id, state);

	return 0;
}
