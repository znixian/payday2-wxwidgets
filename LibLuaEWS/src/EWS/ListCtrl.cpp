#include "stdafx.h"
#include "EWS/ListCtrl.h"

const char *const ListCtrl::KEY = MT_KEY("ListCtrl");

ListCtrl::ListCtrl(lua_State *L) {
	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);
	wxWindow *parent = other->GetWrappedObject();
	assert(parent);

	int id = wxID_ANY;
	string id_str = luaL_checkstring(L, 2);
	if (!id_str.empty()) {
		id = StringToId(id_str, true);
	}

	// Break down something like CAPTION,SYSTEM_MENU,MINIMIZE_BOX,CLOSE_BOX
	const char *flags_str = luaL_checkstring(L, 3);
	int flags = DecodeFlagsString(L, flags_str);

	WrappedWindowWritable() = new wxListCtrl(parent, id, wxDefaultPosition, wxDefaultSize, flags);
}

void ListCtrl::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<ListCtrl>);
	lua_setfield(L, -2, "ListCtrl");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void ListCtrl::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);

	// List of unimplemented methods from Lua:
	/*
		list_ctrl:freeze
		list_ctrl:thaw

		list_ctrl:image_count
		list_ctrl:set_image_list
		list_ctrl:set_item_image
	*/

#define FUNC(name) REGISTER_LUA_FUNC(ListCtrl, name)
	FUNC(append_column);
	FUNC(append_item);
	FUNC(delete_item);
	FUNC(delete_all_items);
	FUNC(clear_all);
	FUNC(get_item);
	FUNC(set_item);
	FUNC(set_item_bold);
	FUNC(get_item_data);
	FUNC(set_item_data);
	FUNC(autosize_column);
	FUNC(selected_item);
	FUNC(selected_items);
	FUNC(set_item_selected);
	FUNC(item_count);
#undef FUNC
}

int ListCtrl::append_column(lua_State * L) {
	string heading = luaL_checkstring(L, 1);
	// TODO style

	GetWrappedObject()->AppendColumn(heading);

	return 0;
}

int ListCtrl::append_item(lua_State * L) {
	// Make sure we're not missing any args
	if (lua_gettop(L) != 1) {
		luaL_error(L, "Too many args for ListCtrl::set_item");
	}

	string label = luaL_checkstring(L, 1);

	int row = GetWrappedObject()->GetItemCount();
	GetWrappedObject()->InsertItem(row, label);

	lua_pushinteger(L, row);
	return 1;
}

int ListCtrl::delete_item(lua_State * L) {
	// Make sure we're not missing any args
	if (lua_gettop(L) != 1) {
		luaL_error(L, "Too many args for ListCtrl::set_item");
	}

	int row = luaL_checkint(L, 1);
	GetWrappedObject()->DeleteItem(row);

	return 0;
}

int ListCtrl::delete_all_items(lua_State * L) {
	GetWrappedObject()->DeleteAllItems();
	return 0;
}

int ListCtrl::clear_all(lua_State * L) {
	GetWrappedObject()->ClearAll();
	return 0;
}

int ListCtrl::get_item(lua_State * L) {
	int row = luaL_checknumber(L, 1);
	int col = luaL_checknumber(L, 2);

	wxString text = GetWrappedObject()->GetItemText(row, col);

	lua_pushstring(L, text.c_str());
	return 1;
}

int ListCtrl::set_item(lua_State * L) {
	int row = luaL_checknumber(L, 1);
	int col = luaL_checknumber(L, 2);
	string label = luaL_checkstring(L, 3);

	GetWrappedObject()->SetItem(row, col, label);

	return 0;
}

int ListCtrl::set_item_bold(lua_State * L) {
	// Make sure we're not missing any args
	if (lua_gettop(L) != 2) {
		luaL_error(L, "Too many args for ListCtrl::set_item");
	}

	int row = luaL_checknumber(L, 1);
	bool bold = lua_toboolean(L, 2);

	wxFont font = GetWrappedObject()->GetItemFont(row);
	font.SetWeight(bold ? wxFONTWEIGHT_BOLD : wxFONTWEIGHT_NORMAL);
	GetWrappedObject()->SetItemFont(row, font);

	return 0;
}

int ListCtrl::get_item_data(lua_State * L) {
	int row = luaL_checkint(L, 1);

	// Returns 0 if we haven't already set a value
	int ref = (int) GetWrappedObject()->GetItemData(row);

	// 0 is the reference to 'nil', so this defaults to nil
	lua_rawgeti(L, LUA_REGISTRYINDEX, ref);

	return 1;
}

int ListCtrl::set_item_data(lua_State * L) {
	int row = luaL_checkint(L, 1);
	int r = luaL_ref(L, LUA_REGISTRYINDEX);

	GetWrappedObject()->SetItemData(row, r);

	return 0;
}

int ListCtrl::autosize_column(lua_State * L) {
	// Make sure we're not missing any args
	if (lua_gettop(L) != 1) {
		luaL_error(L, "Too many args for ListCtrl::set_item");
	}

	int col = luaL_checkint(L, 1);

	GetWrappedObject()->SetColumnWidth(col, wxLIST_AUTOSIZE);

	return 0;
}

int ListCtrl::selected_item(lua_State * L) {
	int itemIndex = -1;
	while ((itemIndex = GetWrappedObject()->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
		// Got the selected item index

		lua_pushinteger(L, itemIndex);
		return 1;
	}

	// Return nothing
	// TODO is this the correct action?
	return 0;
}

int ListCtrl::selected_items(lua_State * L) {
	lua_newtable(L);

	int i = 1;

	int itemIndex = -1;
	while ((itemIndex = GetWrappedObject()->GetNextItem(itemIndex, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != wxNOT_FOUND) {
		lua_pushinteger(L, itemIndex);
		lua_rawseti(L, -2, i++);
	}

	return 1;
}

int ListCtrl::set_item_selected(lua_State *L) {
	int row = luaL_checkint(L, 1);
	bool selected = lua_toboolean(L, 2);

	if (selected) {
		GetWrappedObject()->SetItemState(row, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
	}
	else {
		GetWrappedObject()->SetItemState(row, 0, wxLIST_STATE_SELECTED | wxLIST_STATE_FOCUSED);
	}

	return 0;
}

int ListCtrl::item_count(lua_State * L) {
	int count = GetWrappedObject()->GetItemCount();

	lua_pushinteger(L, count);
	return 1;
}
