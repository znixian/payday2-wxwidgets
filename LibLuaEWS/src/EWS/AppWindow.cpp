#include "stdafx.h"
#include "EWS/AppWindow.h"
#include "WXEvents.h"

const char *const AppWindow::KEY = MT_KEY("AppWindow");

AppWindow::AppWindow(lua_State *L) {
	static const char* type_opt[] = {
		"HORIZONTAL", "VERTICAL", NULL
	};
	static int type_opt_map[] = {
		wxHORIZONTAL, wxVERTICAL
	};

	Window *other = *(Window**)luaX_checkudata(L, 1, Window::KEY);

	wxWindow * parent = other->GetWrappedObject();
	assert(parent);
	WrappedWindowWritable() = new wxPanel(parent);
}

void AppWindow::AddLua(lua_State * L) {
	lua_pushcfunction(L, lua_new_cxxobject<AppWindow>);
	lua_setfield(L, -2, "AppWindow");

	luaX_newmetatable(L, KEY);

	lua_pushstring(L, "__index");
	lua_pushvalue(L, -2);  /* pushes the metatable */
	lua_settable(L, -3);  /* metatable.__index = metatable */

	AddMembers(L);

	lua_pop(L, 1);
}

void AppWindow::AddMembers(lua_State * L) {
	luaX_addparent(L, KEY);
	Window::AddMembers(L);

#define FUNC(name) REGISTER_LUA_FUNC(AppWindow, name)
	FUNC(set_max_size);
#undef FUNC
}

int AppWindow::set_max_size(lua_State * L) {
	// TODO implement

	return 0;
}

// TODO is this now unnecessary since the main connect function checks for an ID?
int AppWindow::lua_connect(lua_State *L) {
	string eventString = luaL_checkstring(L, 1);

	events::EventUserdata *data = new events::EventUserdata;
	data->L = L;
	data->idString = "";
	luaL_checktype(L, 2, LUA_TFUNCTION);
	lua_pushvalue(L, 2);
	data->callback = luaL_ref(L, LUA_REGISTRYINDEX);

	lua_pushvalue(L, 3);
	data->arg = luaL_ref(L, LUA_REGISTRYINDEX); // Not sure we need to do this as I think only strings and numbers are passed, but whatever.

	if (!GetWXEvents().count(eventString)) {
		luaL_error(L, "Unknown event %s", eventString.c_str());
	}

	wxEventType eventId = GetWXEvents().at(eventString);

	assert(GetWrappedEvtHandler(), "No wrapped event handler in AppWindow::lua_connect!");

	// TODO are we supposed to disconnect any previous callbacks ore anything like that?

	GetWrappedEvtHandler()->Bind(eventId, &AppWindow::HandleEvent, this, wxID_ANY, wxID_ANY, data);

	// TODO
	return 0;
}
