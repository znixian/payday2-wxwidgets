#pragma once

// Custom Lua functions to use
// For now, these are the metatable functions as I've only found one in the PD2 Windows binary as of yet,
// and they appear incompatible with the vanilla ones so we'll just use them

// One snippet from lauxlib.h that's otherwise not included b/c I couldn't be bothered

#define luaX_getmetatable(L,n) (lua_getfield(L, LUA_REGISTRYINDEX, (n)))
#define lua_isstring(L,n)	(lua_type(L, (n)) == LUA_TSTRING)

// Check and Opt functions, from LAuxLib
// Defined in xlua.h

#define lua_isnumber(L,n)		(lua_type(L, (n)) == LUA_TNUMBER)

#define luaL_argcheck(L, cond,numarg,extramsg)  \
                ((void)((cond) || luaL_argerror(L, (numarg), (extramsg))))
#define luaL_checkstring(L,n)   (luaL_checklstring(L, (n), NULL))
#define luaL_optstring(L,n,d)   (luaL_optlstring(L, (n), (d), NULL))
#define luaL_checkint(L,n)      ((int)luaL_checkinteger(L, (n)))
#define luaL_optint(L,n,d)      ((int)luaL_optinteger(L, (n), (d)))
#define luaL_checklong(L,n)     ((long)luaL_checkinteger(L, (n)))
#define luaL_optlong(L,n,d)     ((long)luaL_optinteger(L, (n), (d)))

#define luaL_opt(L,f,n,d)       (lua_isnoneornil(L,(n)) ? (d) : f(L,(n)))
#define luaL_typename(L,i)      lua_typename(L, lua_type(L,(i)))

int luaL_argerror(lua_State *L, int narg, const char *extramsg);

int luaL_checkoption(lua_State *L, int narg, const char *def, const char *const lst[]);

int luaL_typerror(lua_State *L, int narg, const char *tname);

void luaL_checktype(lua_State *L, int narg, int t);

void luaL_checkany(lua_State *L, int narg);

const char *luaL_checklstring(lua_State *L, int narg, size_t *len);

const char *luaL_optlstring(lua_State *L, int narg,
	const char *def, size_t *len);

lua_Number luaL_checknumber(lua_State *L, int narg);

lua_Number luaL_optnumber(lua_State *L, int narg, lua_Number def);

lua_Integer luaL_checkinteger(lua_State *L, int narg);

lua_Integer luaL_optinteger(lua_State *L, int narg,
	lua_Integer def);

// Reimplemented functions, marked by the 'X' in luaX

int   luaX_newmetatable(lua_State *L, const char *tname);
void  luaX_addparent(lua_State *L, const char *tname); // Custom again
bool  luaX_isudata(lua_State *L, int index, const char *tname);
void *luaX_checkudata(lua_State *L, int index, const char *tname);

// Made these up, they're not in Lua

void *luaX_checkudata_nonil(lua_State *L, int index, const char *tname);
int   luaX_checkoptionmapped(lua_State *L, int narg, const char *def, const char *const lst[], int const mapping[]);

template< class T >
T *luaX_checkudata_cast(lua_State *L, int index) {
	typedef T* Ref;
	//if (lua_isnoneornil(L, index) && allow_nil) return NULL;
	Ref *ref = (Ref*)luaX_checkudata(L, index, T::KEY);
	if (!ref) {
		luaL_typerror(L, index, lua_typename(L, LUA_TUSERDATA));
	}
	return *ref;
}
