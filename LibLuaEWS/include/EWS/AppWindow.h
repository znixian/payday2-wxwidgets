#pragma once
#include "Window.h"

class AppWindow : public Window_Template<wxPanel> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	AppWindow(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	int lua_connect(lua_State *L);
	int set_max_size(lua_State *L);
};
