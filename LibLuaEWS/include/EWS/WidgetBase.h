#pragma once
#include "stdafx.h"

#define MT_KEY(str) ("xyz.znix.EWS.Widgets." str)
#define REGISTER_LUA_FUNC(klass, name) \
lua_pushcfunction(L, (lua_invoke<klass, &klass::name>)); \
lua_setfield(L, -2, #name)

namespace events {
	class EventUserdata : public wxObject {
	public:
		lua_State * L;
		int callback;
		int arg;

		// Other stuff to pass
		string idString;

		// TODO implement destructor to free references
	};
}

class WidgetBase {
public:
	static const char *KEY;
	static void DispatchLuaEventQueue();

	virtual ~WidgetBase();

	virtual const char *GetKey() = 0;
	virtual wxEvtHandler *GetWrappedEvtHandler() = 0;
protected:
	static void AddMembers(lua_State *L);
	static int StringToId(string id, bool allowRegister);

	WidgetBase();

	virtual int lua_connect(lua_State *L);
	void HandleEvent(wxEvent&);
};

template<class T>
class WidgetBase_Template : public WidgetBase {
public:
	~WidgetBase_Template() {
		if (wrappedObject) {
			delete (wxObject*)GetWrappedObject();
		}
	}

	virtual T *GetWrappedObject() { return wrappedObject; }
	operator T*() { return GetWrappedObject(); }
	virtual wxEvtHandler *GetWrappedEvtHandler() { return (wxEvtHandler*)GetWrappedObject(); }
protected:
	virtual T *&WrappedObjectWritable() { return wrappedObject; }
private:
	T * wrappedObject = NULL;
};

// Template functions

template<typename T> int lua_new_cxxobject(lua_State *L) {
	lua_remove(L, 1); // Remove the 'EWS' argument

	T *obj = new T(L);

	luaX_push_cxxobject(L, obj);

	return 1;  /* new userdatum is already on the stack */
}

// TODO should this be in xlua?
template<typename T> void luaX_push_cxxobject(lua_State *L, T *object) {
	typedef T* Ref;

	assert(object); // , "Cannot push null object!");

	Ref *ref = (Ref*)lua_newuserdata(L, sizeof(Ref));
	*ref = object;

	luaX_getmetatable(L, T::KEY);
	lua_setmetatable(L, -2);
}

template<typename T, int(T::*F)(lua_State*)> int lua_invoke(lua_State *L) {
	typedef T* Ref;

	Ref *ref = (Ref*)luaX_checkudata_nonil(L, 1, T::KEY);
	T *obj = *ref;
	lua_remove(L, 1); // Remove the 'self' value

	return (*obj.*F)(L);
}
