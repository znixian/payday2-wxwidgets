#pragma once
#include "WidgetBase.h"

class Window : public WidgetBase_Template<wxWindow> {
public:
	static const char * const KEY;
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int set_visible(lua_State * L);
	int set_sizer(lua_State * L);
	int set_position(lua_State * L);
	virtual int freeze(lua_State * L);
	virtual int thaw(lua_State * L);
	virtual int refresh(lua_State * L);
	virtual int fit(lua_State * L);

	static const char *STYLE_FLAGS_NAMES[];
	static int STYLE_FLAGS_MASKS[];
	static int DecodeFlagsString(lua_State *L, const char *str);
};

template<class T>
class Window_Template : public Window {
public:
	virtual wxWindow * GetWrappedWindow() { return wrappedObject; }
	virtual T * GetWrappedObject() override { return wrappedObject; }
	operator T*() { return GetWrappedObject(); }
protected:
	virtual T * &WrappedWindowWritable() { return wrappedObject; }
	virtual wxWindow * &WrappedObjectWritable() override { throw "Cannot set wrapped Window object"; } // TODO trigger a warning on usage or something
private:
	T * wrappedObject;
};
