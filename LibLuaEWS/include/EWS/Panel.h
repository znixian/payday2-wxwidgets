#pragma once
#include "Window.h"

class Panel : public Window_Template<wxPanel> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	Panel(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);
};
