#pragma once
#include "Window.h"
#include "wx/listctrl.h"

class ListCtrl : public Window_Template<wxListCtrl> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	ListCtrl(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int append_column(lua_State *L);
	int delete_all_items(lua_State *L);
	int clear_all(lua_State * L);
	int append_item(lua_State *L);
	int delete_item(lua_State * L);
	int get_item(lua_State * L);
	int set_item(lua_State *L);
	int set_item_bold(lua_State *L);
	int get_item_data(lua_State * L);
	int set_item_data(lua_State * L);
	int autosize_column(lua_State *L);
	int selected_item(lua_State *L);
	int selected_items(lua_State * L);
	int set_item_selected(lua_State * L);
	int item_count(lua_State * L);
};
