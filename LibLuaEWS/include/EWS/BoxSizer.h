#pragma once
#include "WidgetBase.h"

class BoxSizer : public WidgetBase_Template<wxBoxSizer> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	BoxSizer(lua_State *L);
	~BoxSizer();

	virtual const char *GetKey() { return KEY; }
	virtual wxEvtHandler *GetWrappedEvtHandler() { throw "Not an event handler!"; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	virtual int add(lua_State *L);
};
