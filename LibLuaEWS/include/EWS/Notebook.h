#pragma once
#include "Window.h"
#include "wx/notebook.h"

class Notebook : public Window_Template<wxNotebook> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	Notebook(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int add_page(lua_State *L);
	int delete_page(lua_State * L);
	int delete_all_pages(lua_State * L);
	int get_page(lua_State *L);
	int get_page_text(lua_State * L);
	int set_page(lua_State * L);
	int set_page_text(lua_State * L);
	int get_current_page(lua_State *L);
	int get_page_count(lua_State * L);
};
