#pragma once
#include "Window.h"

class MenuBar : public Window_Template<wxMenuBar> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	MenuBar(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int append(lua_State *L);
};

class Menu : public WidgetBase_Template<wxMenu> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	Menu(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int append_item(lua_State *L);
	int append_separator(lua_State *L);
	int append_check_item(lua_State *L);
	int append_radio_item(lua_State *L);
	int append_menu(lua_State *L);
	int is_checked(lua_State *L);
	int set_checked(lua_State *L);
};
