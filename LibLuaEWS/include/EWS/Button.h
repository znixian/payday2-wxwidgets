#pragma once
#include "Window.h"

class Button : public Window_Template<wxButton> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	Button(lua_State *L);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);
};
