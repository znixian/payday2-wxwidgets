#pragma once
#include "WidgetBase.h"

class LuaWXEvent {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	LuaWXEvent(string id);

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int get_id(lua_State *L);

private:
	// Variables
	string id;
};
