#pragma once
#include "Window.h"

class Frame : public Window_Template<wxFrame> {
public:
	static const char * const KEY;
	static void AddLua(lua_State *L);

	Frame(lua_State *L);
	~Frame();

	virtual const char *GetKey() { return KEY; }
protected:
	static void AddMembers(lua_State *L);

	// Lua functions
	int set_menu_bar(lua_State *L);
	int set_focus(lua_State *L);
	int set_icon(lua_State *L);
};
