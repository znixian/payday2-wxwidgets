#pragma once

struct dslScriptVector3 {
	float x;
	float y;
	float z;

	// TODO alignment stuff

	// Remove the constructor, as it'd be missing whatever important stuff is required
	dslScriptVector3() = delete;
	dslScriptVector3(dslScriptVector3&) = delete;

	operator wxPoint() const { return wxPoint((int)x, (int)y); }
	operator wxSize() const { return wxSize((int)x, (int)y); }
};
